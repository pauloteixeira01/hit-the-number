import react, { useState } from 'react';
import './App.css';

function App() {
  // INPUT, RUNNING and END
  const [ state, setState ] = useState('INPUT');

  // HUNCH
  const [ hunch, setHunch ] = useState(150);

  // NUMBER OF HUNCHES
  const [numHunch, setNumHunch] = useState(1);

  // MINIMUM AND MAXIMUM
  const [ min, setMin ] =useState(0);
  const [ max, setMax ] =useState(300);


  function start() {
    setState("RUNNING");
    setMin(0);
    setMax(300);
    setHunch(150);
    setNumHunch(1);
  }

  if (state === 'INPUT') {
    return (
      <div className="App">
        <button onClick={start}>START</button>
      </div>
    );
  };

  // Binary search
  function minimum() {
    setNumHunch(numHunch + 1);  
    setMax(hunch);
    const nextHunch = parseInt((hunch - min) / 2) + min;
    setHunch(nextHunch);
  }

  // Binary search
  function bigger() {
    setNumHunch(numHunch + 1);
    setMin(hunch);
    const nextHunch = parseInt((max - hunch) / 2) + hunch;
    setHunch(nextHunch);
  }

  function hit() {
    setState('THE END');
  }

  if (state === 'THE END') {
    return (
      <div className="App">
        <h1>
          You hit the number <u>{hunch}</u> with <u>{numHunch}</u> guesses.
        </h1>
        <button onClick={start}>RESTART GAME</button>
      </div>
    );
  }
  
  return (
    <div className="App">
      <h1>Is your number {hunch} ?</h1>
      <p>Min: {min} / Max: {max}</p>
      <button onClick={minimum}>SMALLER</button>
      <button onClick={hit}>HIT</button>
      <button onClick={bigger}>BIGGER</button>
    </div>
  );
}

export default App;